import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Playlist from 'App/Models/Playlist'
import Ws from 'App/Services/Ws'

export default class PlaylistsController {
  public async index({ response }: HttpContextContract) {
    const data = await Playlist.find().sort('-order').populate('item')
    return response.json(data)
  }

  public async store({ request, response }: HttpContextContract) {
    const data = await Playlist.create({
      item: request.input('element._id'),
      order: request.input('newIndex'),
      isActive: false
    })

    Ws.io.emit('playlist:add', data)
  }

  public async destroy({ params }: HttpContextContract) {
    await Playlist.findByIdAndRemove(params.id)

    Ws.io.emit('playlist:destroy', params.id)
  }
}
