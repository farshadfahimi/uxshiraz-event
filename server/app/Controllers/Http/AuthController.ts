import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Hash from '@ioc:Adonis/Core/Hash'
import User from 'App/Models/User'

export default class AuthController {
  public async register({ auth, request, response }: HttpContextContract) {
    await request.validate({
      schema: schema.create({
        email: schema.string({ trim: true }, [rules.email()]),
        password: schema.string({ trim: true }, [ rules.minLength(6) ])
      })
    })

    const password = await Hash.make(request.input('password'))

    const user = await User.create({
      name: request.input('name'),
      email: request.input('email'),
      password
    })

    const token = await auth.use('api').login(user)
    return response.json(token)
  }

  public async login({ auth, request, response }: HttpContextContract) {
    await request.validate({
      schema: schema.create({
        email: schema.string({ trim: true }, [rules.email()]),
        password: schema.string({ trim: true }, [rules.minLength(6)])
      })
    })

    const user = await User.findOne({ email: request.input('email') })

    if (!user)
      return response.status(404).json({ errors: [{ message: 'کاربری یافت نشد' }] })

    if (await Hash.verify(user.password, request.input('password'))) {
      const token = await auth.use('api').login(user)

      return response.status(200).json(token)
    }

    return response.status(400).json({ errors: [{ message: 'خطا در ارسال اطلاعات' }] })
  }

  public async profile({ auth }: HttpContextContract) {
    return { user: auth.user}
  }
}
