import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Pin from 'App/Models/Pin'

export default class PinsController {
  public async index({ response }: HttpContextContract) {
    const pins = await Pin.find().sort('-order')

    return response.json(pins)
  }

  public async store({ request, response }: HttpContextContract) {
    await request.validate({
      schema: schema.create({
        title: schema.string({ trim: true }, [rules.required()]),
        logo: schema.string({ trim: true }, [rules.required()]),
        url: schema.string({ trim: true }, [rules.required()])
      })
    })

    const pin = await Pin.create(request.only(['title', 'logo', 'url']))

    return response.json({
      data: pin
    })
  }

  public async update({ params, request }: HttpContextContract) {
    await Pin.findOneAndUpdate({ _id: params.id }, { 
      title: request.input('title'),
      thumb_url: request.input('thumb_url'),
      link: request.input('link'),
    })
  }

  public async destroy({ params }: HttpContextContract) {
    await Pin.deleteOne({ _id: params.id })
  }
}
