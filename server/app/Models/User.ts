import { Schema, model } from '@ioc:Mongoose'

export enum Role {
  USER = 'USER',
  ADMIN = 'ADMIN'
}

export default model('User', new Schema({
  name: String,
  avatar: String,
  role: {
    type: String,
    enum: Role,
  },
  email: String,
  password: String,
}))