import { Schema, model } from '@ioc:Mongoose'

export default model('Item', new Schema({
  title: String,
  name: String,
  type: {
    type: String,
    enum: ['VIDEO', 'AUDIO', 'IMAGE', 'LIVE']
  },
  hasFaq: Boolean,
  media: String,
  thumb: String,
}))
