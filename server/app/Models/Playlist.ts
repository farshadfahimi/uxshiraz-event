import { Schema, model } from '@ioc:Mongoose'
import Item from 'App/Models/Item'

export default model('Playlist', new Schema({
  item: {
    type: Schema.Types.ObjectId,
    ref: Item,
  },
  order: Number,
  isActive: Boolean
}))