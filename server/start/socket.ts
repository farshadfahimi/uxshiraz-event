import Ws from 'App/Services/Ws'
Ws.boot()

let activeItem: Array<any> = []

/**
 * Listen for incoming socket connections
 */

 Ws.io.on('connection', (socket) => {
   socket.on('plan:add', response => {
      activeItem = [response]

      socket.broadcast.emit('plan:activate', activeItem)
   })

   socket.on('plan:active', () => {
      socket.emit('plan:active', activeItem)
   })

   socket.on('plan:refresh', () => {
      socket.broadcast.emit('plan:refresh', activeItem)
   })
})
