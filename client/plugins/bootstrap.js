import Vue from 'vue'

import {
  BButton,
  BNavbar,
  BNav,
  BNavItem,
  BNavbarBrand,
  BFormCheckbox,
  BIconCloudUpload,
  BFormRadio,
  BFormRadioGroup,
  BDropdown,
  BDropdownItem,
  BIcon,
  BIconArrowRepeat,
  BIconChevronDown,
  BIconChevronLeft,
  BIconLink45deg,
  BIconGrid,
  BIconPerson,
  BIconDoorOpen,
  BIconPlay,
  BIconSkipForwardFill,
  BIconWifi,
  BIconCircleFill,
  BSpinner,
  BModal,
  BToast,
  ModalPlugin,
  ToastPlugin
} from 'bootstrap-vue'

import Avatar from '@/components/Avatar'
import AppSidebar from '@/components/Sidebar'
import MediaPlayer from '@/components/MediaPlayer'

Vue.component('Avatar', Avatar)
Vue.component('AppSidebar', AppSidebar)
Vue.component('MediaPlayer', MediaPlayer)
Vue.component('BButton', BButton)
Vue.component('BNavbar', BNavbar)
Vue.component('BNav', BNav)
Vue.component('BNavItem', BNavItem)
Vue.component('BNavbarBrand', BNavbarBrand)
Vue.component('BFormRadio', BFormRadio)
Vue.component('BFormRadioGroup', BFormRadioGroup)
Vue.component('BFormCheckbox', BFormCheckbox)
Vue.component('BIconCloudUpload', BIconCloudUpload)
Vue.component('BDropdown', BDropdown)
Vue.component('BIconLink45deg', BIconLink45deg)
Vue.component('BSpinner', BSpinner)
Vue.component('BDropdownItem', BDropdownItem)
Vue.component('BIcon', BIcon)
Vue.component('BIconArrowRepeat', BIconArrowRepeat)
Vue.component('BIconChevronDown', BIconChevronDown)
Vue.component('BIconChevronLeft', BIconChevronLeft)
Vue.component('BIconPerson', BIconPerson)
Vue.component('BIconDoorOpen', BIconDoorOpen)
Vue.component('BIconPlay', BIconPlay)
Vue.component('BIconSkipForwardFill', BIconSkipForwardFill)
Vue.component('BIconWifi', BIconWifi)
Vue.component('BIconCircleFill', BIconCircleFill)
Vue.component('BIconGrid', BIconGrid)
Vue.component('BModal', BModal)
Vue.component('BToast', BToast)

Vue.use(ModalPlugin)
Vue.use(ToastPlugin)