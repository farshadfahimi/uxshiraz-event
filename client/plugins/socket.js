import { io } from 'socket.io-client'

const socket = io(process.env.wsUrl, {
  forceNew: false,
  autoConnect: true,
  transports: ['websocket'],
  reconnection: true,
})

export default (_ctx, inject) => {
  socket.io.on('connection', (socket) => {
    console.log(socket)
  })

  socket.io.on('error', (e) => {
    console.log(e)
  })
  inject('socket', socket)
}