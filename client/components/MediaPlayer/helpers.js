/**
 * Video js player options 
 */
export const playerOptions = {
  autoplay: true,
  controls: false,
  // html5: {
  //   vhs: {
  //     enableLowInitialPlaylist: true,
  //   }
  // },
  controlBar: {
    timeDivider: false,
    durationDisplay: false
  }
}