export const state = () => ({
  activeTab: 'ITEM'
})

export const mutations = {
  SET_TAB(state, payload) {
    state.activeTab = payload
  }
}